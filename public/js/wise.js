var jumboHeight = $('.jumbotron').outerHeight();
//$('.bg-jumbo').css('margin-top',$('.jumbotron').position().top);
function sending() {
 var $contactFormIcon= $(".contact-form-icon i");
    $contactFormIcon
        .removeClass('fa-telegram fab')
        .addClass('fas fa-cog fa-spin');
    var title = $(".contact-container h2 .title-text");
    title.text("Contacting");
    grecaptcha.ready(function() {
        grecaptcha.execute('6LfFpocUAAAAAJEyRqzsqlwQ6adUO9TwIGjI9awO', {action: 'sendmail'});
    });
}

function noSending() {
    var $contactFormIcon= $(".contact-form-icon i");
    $contactFormIcon
        .removeClass('fa-cog fa-spin fas')
        .addClass('fab fa-telegram');
    var title = $(".contact-container h2 .title-text");
    title.text("Contact");
}

function parallax(){
    var scrolled = $(window).scrollTop(),
        mainMenuHeight = $("#main-menu").outerHeight();

    $('.bg-jumbo').css('height', (jumboHeight-scrolled+mainMenuHeight) + 'px');
}

function processMailSent(message) {
    var cni = $("input#yourName");
    var cei = $("input#yourEmail");
    var cpi = $("input#yourPhone");

    if(message=='OK') {
        cni.prop('disabled',false).val("");
        cpi.prop('disabled',false).val("");
        cei.prop('disabled',false).val("");
        $('form[name=contact] button').addClass('teal');
    }
    else
        alert(message);
    noSending();
}

$(window).scroll(function(e){
    parallax();
});

parallax();

$("form[name=contact]").submit(function(e) {
    e.preventDefault();
    var cni = $(this).find("input#yourName");
    var cei = $(this).find("input#yourEmail");
    var cpi = $(this).find("input#yourPhone");
    var contactName = cni.val();
    var contactEmail = cei.val();
    var contactPhone = cpi.val();
    cpi.prop('disabled',true);
    cei.prop('disabled',true);
    cni.prop('disabled',true);

    var to = 'fernando.errandosoro@gmail.com';

    sending();
    Email.send({
        //SecureToken: "1b5fe6f2-e629-40b5-92f0-1584b04c0a13",
        SecureToken: "d7e8d4aa-4828-415a-8dc7-7e7d6a816625",
        To : to,
        From : "lucas.vidaguren@asivas.com.ar",
        Subject : "Contacto desde el sitio wise4Sports "+contactName+"<"+contactEmail+">" ,
        Body : "Hola, hay un nuevo contacto desde el sitio Wise.<br> <strong>Nombre</strong> "+contactName.toUpperCase()+" <br><strong>Email:</strong> "+contactEmail+"  <br> <strong>Tel:</strong> "+contactPhone+"  <br>Completó el fomrulario de contacto ("+new Date().toISOString()+") para solicitar más inofmración sobre wise4sports"
    }).then(
        message => processMailSent(message)
    );
    });